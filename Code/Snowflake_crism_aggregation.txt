#Merging mcdash and pennymac EBO data to get new mcdash data
create table "DATASCIENCE"."SANDBOX"."MCDASH_EBO_1" as 
select * from "DATASCIENCE"."SANDBOX"."PENNYMAC_EBO_1" PN
left join (
SELECT DISTINCT pnmacloannumber, LOANID as mcdashid   FROM "INDUSTRYTRENDS_PROD"."MCDASH"."LOANOWNERLOOKUP" WHERE PNMACLOANNUMBER IS NOT NULL
) mcdash
ON mcdash.PNMACLOANNUMBER = PN.LOANID
order by PN.loanid desc


#Merge new mcdash and crism data 
create table "DATASCIENCE"."SANDBOX"."CRISM_EBO_ALL_2" as 
select CONCAT(SUBSTRING(EFX_DATE, 0,4),SUBSTRING(EFX_DATE, 6,2))  as YEARMONTH, mcdash.* , crism.*
from "DATASCIENCE"."SANDBOX"."MCDASH_EBO_1" mcdash
left join "CRISM"."PNMAC"."VW_EFX_CRI_PRIMARY_BORROWER" crism
ON mcdash.mcdashid =  crism.LPID 
order by crism.LPID desc, crism.EFX_PERIOD asc 


#To avoid data leakage we need to take crism data which is one month older than FUNDINGYEARMONTH
#Funding year month is the date of modification of the loan. Once we cross that date, we have bought 
#it so we want to simulate a scenario before we make the buy decision 
create table "DATASCIENCE"."SANDBOX"."CRISM_EBO_1" as
select * from  "DATASCIENCE"."SANDBOX"."CRISM_EBO_ALL_2"
where FUNDINGYEARMONTH > YEARMONTH

#Adding a unique ID to uniqely identify each loan 
ALTER TABLE "DATASCIENCE"."SANDBOX"."CRISM_EBO_1"
ADD UNIQUE_ID varchar(20) AS CONCAT(LOANID,FUNDINGYEARMONTH) 

#Grouping by unique id to get the last available observation date in crism
create table "DATASCIENCE"."SANDBOX"."BYPROD_1" as
select max(YEARMONTH) as max_yearmonth, unique_id 
from "DATASCIENCE"."SANDBOX"."CRISM_EBO_1" tab1 
group by unique_id 


#Getting the last available crism observation for each unique id
select tab1.*, tab2.* from "DATASCIENCE"."SANDBOX"."BYPROD_1" tab1
left join "DATASCIENCE"."SANDBOX"."CRISM_EBO_1" tab2 
on tab1.unique_id = tab2.unique_id and tab1.MAX_YEARMONTH = tab2.YEARMONTH


#This is a table with all the non aggregated columns (only interested in latest observation )
create table "DATASCIENCE"."SANDBOX"."CRISM_EBO_2" as 
select tab2.* from "DATASCIENCE"."SANDBOX"."BYPROD_1" tab1
left join "DATASCIENCE"."SANDBOX"."CRISM_EBO_1" tab2 
on tab1.unique_id = tab2.unique_id and tab1.MAX_YEARMONTH = tab2.YEARMONTH

#Drop unnecesary tables 
drop table "DATASCIENCE"."SANDBOX"."BYPROD_1"


#Create a table with aggregations only
create table "DATASCIENCE"."SANDBOX"."CRISM_EBO_Aggs" as 
select LPID as LPID_agg
,replace(listagg(NVL(BCN09, -1.0), ',') within group(order by PERIOD),'','') as BCN09
,replace(listagg(NVL(BCN50, -1.0), ',') within group(order by PERIOD),'','')as BCN50
,replace(listagg(NVL(BCM09, -1.0), ',') within group(order by PERIOD),'','')as BCM09
,replace(listagg(NVL(BNI4, -1.0), ',') within group(order by PERIOD),'','')as BNI4
,replace(listagg(NVL(Vantage, -1.0), ',') within group(order by PERIOD),'','')as Vantage
,replace(listagg(NVL(Vantage2, -1.0), ',') within group(order by PERIOD),'','')as Vantage2
,replace(listagg(NVL(Vantage3, -1.0), ',') within group(order by PERIOD),'','')as Vantage3
,replace(listagg(NVL(PIM, -1.0), ',') within group(order by PERIOD),'','')as PIM
,replace(listagg(NVL(EDTI, -1.0), ',') within group(order by PERIOD),'','')as EDTI
,replace(listagg(EFX_DATE, ',') within group(order by PERIOD),'','') as EFX_DATE
from "DATASCIENCE"."SANDBOX"."CRISM_EBO_1"
GROUP BY LPID
order by MAX(PERIOD) asc;

#Dropping the columns that are aggregated 
ALTER TABLE "DATASCIENCE"."SANDBOX"."CRISM_EBO_2"
DROP COLUMN BCN09, BCN50, BCM09, BNI4, VANTAGE, VANTAGE2, VANTAGE3,PIM, EDTI, EFX_DATE


#Creating a final crism dataset with aggregated columns 
create table "DATASCIENCE"."SANDBOX"."CRISM_EBO_3" as 
select crism_agg.*, crism_2.*
from "DATASCIENCE"."SANDBOX"."CRISM_EBO_2" crism_2
left join "DATASCIENCE"."SANDBOX"."CRISM_EBO_Aggs" crism_agg
on crism_2.lpid = crism_agg.LPID_AGG


#Dropping the duplicate columns 
ALTER TABLE "DATASCIENCE"."SANDBOX"."CRISM_EBO_3"
DROP COLUMN LPID_AGG,LOANID

#Drop unnecesary tables
drop table "DATASCIENCE"."SANDBOX"."CRISM_EBO_Aggs"






