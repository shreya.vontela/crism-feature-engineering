# CRISM-Feature Engineering

## Objective: 
- In this project we join the CRISM data to Pennymac EBO data which gives us a new dataset where we have multiple monthly observations for each unique loan and then we create additional features from the time series of data available in these monthly observations. 

## About the Data 
### Pennymac EBO Dataset 
- This is a set of loans which went into forebarance and we are planning to buy them out of the pool at par value and resell them at a premium once they start making six consequtive payments. In this dataset, each unique loan can be identified by a combination of loan id and funding year month date. Funding year month date is the date on which we decide to buy/modify this loan. 
Click [here](https://docs.google.com/spreadsheets/d/1s-4o2Bn1sBKG5tbJhxBBai3hYS85-Rt5_L8acij4hKY/edit#gid=585930834) to look at the EBO data dictionary.

### CRISM dataset 
- CRISM is a loan dataset which contains monthly observations of each loan. A loan needs to be nine months old before it gets into the CRISM data. 
- Click [here](https://docs.google.com/spreadsheets/d/1IhOSW_Y880w2gw0fysy39OVigHv4aekmLnkjchPO54s/edit#gid=0) to look at the CRISM data dictionary

## Methodology 
### STEP 1: Data Aggregation process in Snowflake 
- We start off by uploading the pennymac EBO dataset to our sandbox in snowflake. Then we do a left join on the EBO dataset with mcdash (on Loanid) and crism data(on LpID)
- To avoid data leakage, we are only interested in the observations prior to the funding year month, so we filter out the crism observations upto (funding year month - 1)  date. 
- We now pick the crism variables on which we want to perform the feature engineering and create a string by aggregating all of the monthly observations. 

### STEP 2: Feature Engineering process in Python 
- We connect to snowflake and access the output file created above and use the feature engineering functions to create a ton of new features to extract maximum possible information from the given data. 

![](/Images/variables.PNG)
